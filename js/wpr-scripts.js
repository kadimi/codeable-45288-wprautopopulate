/**
 * WPRiders script to auto-populate checkout form from Gravity Form fields.
 */
jQuery(document).ready(function () {
    var addressPostcode = jQuery('.woocommerce-checkout .wc_address_validation_postcode_lookup .form-row-first input');
    var addressValue = addressPostcode.val();
    if (addressPostcode.length == 0 || addressValue === '' || addressValue === undefined) {
        //console.log('no value');
        var postcode = jQuery('input#shipping_postcode').val();
        jQuery(addressPostcode).val(postcode);
    }

	jQuery(".woocommerce-checkout").on("select2-selecting", function(e) {
        if(jQuery('input[name="wc_address_validation_postcode_lookup_postcode"]').length){
            jQuery('input[name="wc_address_validation_postcode_lookup_postcode"]').css("border", "1px solid green");
        }
	});

    jQuery('#billing_first_name, #billing_last_name, #billing_email, #billing_phone').each(function() {
          if (jQuery(this).val() == '') {
            jQuery(this).attr("style", "border: 1px solid red !important")
          } else {
            jQuery(this).css("border", "1px solid green");
          }
    });
});