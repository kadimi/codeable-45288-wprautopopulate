<?php

/**
 * The plugin file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @since             1.0.0
 * @package           WPRAutoPopulate
 *
 * @wordpress-plugin
 * Plugin Name:       WPRAutoPopulate
 * Description:       This plugin has been created to auto-populate checkout page fields from any Gravity Form
 * Version:           1.0.2
 * Author:            Felicia Panaite from WPRiders.com
 * Author URI:        http://www.wpriders.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wprautopopulate
 * Domain Path:       /languages
 */
require_once(ABSPATH .'wp-includes/pluggable.php');
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * WPRiders custom enqueue_script
 */
add_action( 'wp_enqueue_scripts', 'wpr_load_scripts', 999 );
/**
 * Load scripts
 */
function wpr_load_scripts() {
    wp_enqueue_script( 'wpr-scripts-js', plugins_url( '/js/wpr-scripts.js', __FILE__ ), array('jquery'), '1.0.2', true );
    wp_enqueue_script( 'wpr-scripts-js');
}

/**
 * WPRiders script to auto-populate checkout form from Gravity Form fields.
 */

if (!isset($_COOKIE['buyer_user'])) {
    // create unique identification ID
    $user_nonce = wp_create_nonce('user-'.time());
    // save to cookies
    setcookie('buyer_user', $user_nonce, 0, '/', ''); // cookie will disapear when browser is closed
}

add_action('gform_after_submission', 'wpr_add_entry_to_db', 10, 2);
function wpr_add_entry_to_db($entry, $form) {
    global $woocommerce;

    $woocommerce->customer->set_shipping_postcode( $entry[10] );
    $toSave = array(
        'id_gf' => $entry['id'],
        'id_session' => $_COOKIE['buyer_user'],
        'wpr_data' => current_time( 'mysql' )
    );

    global $wpdb;

    $wpdb->insert($wpdb->prefix.'wpr_buyer',
        $toSave
    );
}

add_filter('woocommerce_form_field_args', 'wpr_change_default_value', 10, 3);
function wpr_change_default_value($args, $key, $value) {
    global $wpdb, $woocommerce;

    $tablename = $wpdb->prefix.'wpr_buyer';
    $user_nonce = $_COOKIE['buyer_user'];
    $get_results = $wpdb->get_row("SELECT `id_gf` FROM `$tablename` WHERE `id_session` = '$user_nonce' ORDER BY `wpr_data` DESC LIMIT 1");

    if($get_results){

        $entry_id = $get_results->id_gf;
        $entry = GFAPI::get_entry( $entry_id );

        if ('billing_first_name' === $key)
            $args['default'] = $entry[16];
        if ('billing_last_name' === $key)
            $args['default'] = $entry[17];
        if ('billing_email' === $key)
            $args['default'] = $entry[15];
        if ('billing_phone' === $key)
            $args['default'] = $entry[14];
        if ('shipping_postcode' === $key) {
            $args['default'] = $entry[10];
            $woocommerce->customer->set_shipping_postcode( $entry[10] );
        }
    }
    return $args;
}

/**
 * WPRiders script to move "I've read and accept the Terms and Conditions" checkbox
 * at the first step of checkout page.
 */

add_action('woocommerce_after_order_notes', 'wpr_move_terms_and_conditions', 90);
function wpr_move_terms_and_conditions() {
    ?>
    <p class="form-row terms wc-terms-and-conditions">
        <input type="checkbox" class="input-checkbox" name="terms" <?php checked( apply_filters( 'woocommerce_terms_is_checked_default', isset( $_POST['terms'] ) ), true ); ?> id="terms" />
        <label for="terms" class="checkbox"><?php printf( __( 'I&rsquo;ve read and accept the <a href="%s" target="_blank">terms &amp; conditions</a>', 'woocommerce' ), esc_url( wc_get_page_permalink( 'terms' ) ) ); ?> <span class="required">*</span></label>
        <input type="hidden" name="terms-field" value="1" />
    </p>
    <?php
}

